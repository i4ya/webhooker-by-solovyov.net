# От автора 

http://solovyov.net/blog/2013/webhooker/

Когда ты хостишь репозитории сам, то можешь сделать какие угодно хуки и по push’у в свой репозиторий делать что угодно – например, рендерить свой сайт. Но когда ты перемещаешь эти репозитории на GitHub, приходится пушить изменения и туда, и себе на сервер, чтоб они там отрендерились. Хостить сайт на гитхабе мне не хочется, да и это, опять же, лишняя работа – надо будет рендерить его в ветке gh-pages, никакого фана.

В какой-то момент мне надоело (на эту фразу уже можно прямо макрос себе завести, “сделать новый пост с ‘мне надоело’” :)), и я написал вебшлюху – такую маленькую, на 250 строк, программу на Go, которая слушает себе определëнный порт, и когда ей приходит вебхук от гитхаба, исполняет заданную команду. Написал я еë потому, что в инете такой не нашëл (нашëл нерабочую на питоне с кошмарным кодом) – что довольно странно.

Работает она просто:
```
webhooker -p 1234 piranha/webhooker:master='echo new stuff arrived!'
```
Всë, что до = – это регулярное выражение, которое применяется к строке user/repo:branch пришедшего вебхука, а после = – команда, которую нужно выполнить. Таких правил можно указать сколько угодно, а когда их задолбает писать аргументами – можно перенести в любой файл (по одному правилу на строку) и использовать его как webhooker -c path/to/rules.

Работает безукоризненно, память не пожирает почëм зря (в рабочем состоянии после месяца работы – 2 Мб RSS), что еще надо? :–) Enjoy!

# webhooker

An application to run shell commands on incoming WebHooks from Github.

[![Build Status](https://travis-ci.org/piranha/webhooker.png)](https://travis-ci.org/piranha/webhooker)

## Installation

Install it with `go get github.com/piranha/webhooker` or download a binary:

| Linux         | OS X          | Windows       |
|:--------------|:--------------|:--------------|
| [64 bit][l64] | [64 bit][x64] | [64 bit][w64] |

[l64]: http://solovyov.net/files/webhooker-linux
[x64]: http://solovyov.net/files/webhooker-osx
[w64]: http://solovyov.net/files/webhooker-win.exe

## Usage

You run it like this (run webhooker without arguments to get help - you could
also put all rules in a separate config file):

```
./webhooker -p 3456 -i 127.0.0.1 piranha/webhooker:master='echo $COMMIT'
```

It runs every command in `sh`, so you can use more complex commands (with `&&`
and `|`).

`user/repo:branch` pattern is a regular expression, so you could do
`user/project:fix.*=cmd` or even `.*=cmd`.

## Running

I expect you to run it behind your HTTP proxy of choice, and in my case it's
nginx and such config is used to protect it from unwanted requests:

```
    location /webhook {
        proxy_pass http://localhost:3456;
        allow 204.232.175.64/27;
        allow 192.30.252.0/22;
        deny all;
    }
```

After that I can put `http://domain.my/webhook/` in Github's repo settings
WebHook URLs and press 'Test Hook' to check if it works.

## Environment

webhooker provides your commands with some variables in case you need them:

- `$REPO` - repository name in "user/name" format
- `$REPO_URL` - full repository url
- `$PRIVATE` - strings "true" or "false" if repository is private or not
- `$BRANCH` - branch name
- `$COMMIT` - last commit hash id
- `$COMMIT_MESSAGE` - last commit message
- `$COMMIT_TIME` - last commit timestamp
- `$COMMIT_AUTHOR` - username of author of last commit
- `$COMMIT_URL` - full url to commit

And, of course, it passes through some common variables: `$PATH`, `$HOME`,
`$USER`.

## Example

I render my own sites using `webhooker`. I've just put it in
[supervisord](http://supervisord.org/) like that:

```
[program:webhooker]
command = /home/piranha/bin/webhooker -p 5010 -i 127.0.0.1
    piranha/solovyov.net:master='cd ~/web/solovyov.net && GOSTATIC=~/bin/gostatic make update'
    piranha/osgameclones:master='cd ~/web/osgameclones && CYRAX=/usr/local/bin/cyrax make update'
user = piranha
environment=HOME="/home/piranha"
```

You can see that it updates and renders sites on push to them (`make update`
there runs `git pull` and renders site).
